<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stretchings Model
 *
 * @method \App\Model\Entity\Stretching get($primaryKey, $options = [])
 * @method \App\Model\Entity\Stretching newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Stretching[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Stretching|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stretching patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Stretching[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Stretching findOrCreate($search, callable $callback = null)
 */
class StretchingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('stretchings');
        $this->displayField('name');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('list');

        $validator
            ->allowEmpty('img');

        return $validator;
    }
}
