<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pressionpoint'), ['action' => 'edit', $pressionpoint->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pressionpoint'), ['action' => 'delete', $pressionpoint->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pressionpoint->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pressionpoints'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pressionpoint'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pressionpointshistories'), ['controller' => 'Pressionpointshistories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pressionpointshistory'), ['controller' => 'Pressionpointshistories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pressionpoints view large-9 medium-8 columns content">
    <h3><?= h($pressionpoint->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($pressionpoint->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pressionpoint->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($pressionpoint->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($pressionpoint->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pressionpointshistories') ?></h4>
        <?php if (!empty($pressionpoint->pressionpointshistories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Pressionpoint Id') ?></th>
                <th scope="col"><?= __('Val') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pressionpoint->pressionpointshistories as $pressionpointshistories): ?>
            <tr>
                <td><?= h($pressionpointshistories->id) ?></td>
                <td><?= h($pressionpointshistories->user_id) ?></td>
                <td><?= h($pressionpointshistories->pressionpoint_id) ?></td>
                <td><?= h($pressionpointshistories->val) ?></td>
                <td><?= h($pressionpointshistories->created) ?></td>
                <td><?= h($pressionpointshistories->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Pressionpointshistories', 'action' => 'view', $pressionpointshistories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Pressionpointshistories', 'action' => 'edit', $pressionpointshistories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Pressionpointshistories', 'action' => 'delete', $pressionpointshistories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pressionpointshistories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
