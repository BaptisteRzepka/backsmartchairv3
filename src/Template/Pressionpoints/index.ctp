<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pressionpoint'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pressionpointshistories'), ['controller' => 'Pressionpointshistories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pressionpointshistory'), ['controller' => 'Pressionpointshistories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pressionpoints index large-9 medium-8 columns content">
    <h3><?= __('Pressionpoints') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pressionpoints as $pressionpoint): ?>
            <tr>
                <td><?= $this->Number->format($pressionpoint->id) ?></td>
                <td><?= h($pressionpoint->title) ?></td>
                <td><?= h($pressionpoint->created) ?></td>
                <td><?= h($pressionpoint->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pressionpoint->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pressionpoint->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pressionpoint->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pressionpoint->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
