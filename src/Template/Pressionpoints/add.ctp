<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pressionpoints'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pressionpointshistories'), ['controller' => 'Pressionpointshistories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pressionpointshistory'), ['controller' => 'Pressionpointshistories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pressionpoints form large-9 medium-8 columns content">
    <?= $this->Form->create($pressionpoint) ?>
    <fieldset>
        <legend><?= __('Add Pressionpoint') ?></legend>
        <?php
            echo $this->Form->input('title');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
