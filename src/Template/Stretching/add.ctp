<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Stretching'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="stretching form large-9 medium-8 columns content">
    <?= $this->Form->create($stretching) ?>
    <fieldset>
        <legend><?= __('Add Stretching') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('description');
            echo $this->Form->input('list');
            echo $this->Form->input('img');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
