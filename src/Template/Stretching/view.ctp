<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Stretching'), ['action' => 'edit', $stretching->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Stretching'), ['action' => 'delete', $stretching->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stretching->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Stretching'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stretching'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="stretching view large-9 medium-8 columns content">
    <h3><?= h($stretching->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($stretching->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Img') ?></th>
            <td><?= h($stretching->img) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($stretching->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($stretching->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('List') ?></h4>
        <?= $this->Text->autoParagraph(h($stretching->list)); ?>
    </div>
</div>
