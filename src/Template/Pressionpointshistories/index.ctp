<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pressionpointshistory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pressionpoints'), ['controller' => 'Pressionpoints', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pressionpoint'), ['controller' => 'Pressionpoints', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pressionpointshistories index large-9 medium-8 columns content">
    <h3><?= __('Pressionpointshistories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('val') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pressionpoint_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pressionpointshistories as $pressionpointshistory): ?>
            <tr>
                <td><?= $this->Number->format($pressionpointshistory->id) ?></td>
                <td><?= $this->Number->format($pressionpointshistory->val) ?></td>
                <td><?= h($pressionpointshistory->created) ?></td>
                <td><?= h($pressionpointshistory->modified) ?></td>
                <td><?= $pressionpointshistory->has('user') ? $this->Html->link($pressionpointshistory->user->id, ['controller' => 'Users', 'action' => 'view', $pressionpointshistory->user->id]) : '' ?></td>
                <td><?= $pressionpointshistory->has('pressionpoint') ? $this->Html->link($pressionpointshistory->pressionpoint->title, ['controller' => 'Pressionpoints', 'action' => 'view', $pressionpointshistory->pressionpoint->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pressionpointshistory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pressionpointshistory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pressionpointshistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pressionpointshistory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
