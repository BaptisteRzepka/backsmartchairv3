<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pressionpointshistories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pressionpoints'), ['controller' => 'Pressionpoints', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pressionpoint'), ['controller' => 'Pressionpoints', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pressionpointshistories form large-9 medium-8 columns content">
    <?= $this->Form->create($pressionpointshistory) ?>
    <fieldset>
        <legend><?= __('Add Pressionpointshistory') ?></legend>
        <?php
            echo $this->Form->input('val');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('pressionpoint_id', ['options' => $pressionpoints, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
