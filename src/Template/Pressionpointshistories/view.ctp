<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pressionpointshistory'), ['action' => 'edit', $pressionpointshistory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pressionpointshistory'), ['action' => 'delete', $pressionpointshistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pressionpointshistory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pressionpointshistories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pressionpointshistory'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pressionpoints'), ['controller' => 'Pressionpoints', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pressionpoint'), ['controller' => 'Pressionpoints', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pressionpointshistories view large-9 medium-8 columns content">
    <h3><?= h($pressionpointshistory->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $pressionpointshistory->has('user') ? $this->Html->link($pressionpointshistory->user->id, ['controller' => 'Users', 'action' => 'view', $pressionpointshistory->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pressionpoint') ?></th>
            <td><?= $pressionpointshistory->has('pressionpoint') ? $this->Html->link($pressionpointshistory->pressionpoint->title, ['controller' => 'Pressionpoints', 'action' => 'view', $pressionpointshistory->pressionpoint->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pressionpointshistory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Val') ?></th>
            <td><?= $this->Number->format($pressionpointshistory->val) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($pressionpointshistory->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($pressionpointshistory->modified) ?></td>
        </tr>
    </table>
</div>
