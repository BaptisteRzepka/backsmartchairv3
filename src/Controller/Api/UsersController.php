<?php
/**
 * Created by PhpStorm.
 * User: baptisterzepka
 * Date: 17/11/2016
 * Time: 20:48
 */

namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\Datasource\ConnectionManager;

class UsersController extends AppController
{
    public $uses = array('Pressionpointshistory', 'User');

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'token']);
    }

    public function add()
    {
        $this->Crud->on('afterSave', function(Event $event) {
            if ($event->subject->created) {
                $this->set('data', [
                    'id' => $event->subject->entity->id,
                    'token' => JWT::encode(
                        [
                            'sub' => $event->subject->entity->id,
                            'exp' =>  time() + 604800
                        ],
                        Security::salt())
                ]);
                $this->Crud->action()->config('serialize.data', 'data');
            }
        });
        return $this->Crud->execute();
    }

    public function token()
    {
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException('Invalid username or password');
        }

        $this->set([
            'success' => true,
            'data' => [
                'token' => JWT::encode([
                    'sub' => $user['id'],
                    'exp' =>  time() + 604800
                ],
                    Security::salt())
            ],
            '_serialize' => ['success', 'data']
        ]);
    }

    public function lastposition()
    {
        $user = '1';
        $conn = ConnectionManager::get('default');

        $c1 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 1 ORDER BY created DESC');
        $c2 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 2 ORDER BY created DESC');
        $c3 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 3 ORDER BY created DESC');
        $c4 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 4 ORDER BY created DESC');
        $c5 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 5 ORDER BY created DESC');
        $c6 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 6 ORDER BY created DESC');
        $c7 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 7 ORDER BY created DESC');
        $c8 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 8 ORDER BY created DESC');

        $c1r = $c1->fetchAll('assoc');
        $c2r = $c2->fetchAll('assoc');
        $c3r = $c3->fetchAll('assoc');
        $c4r = $c4->fetchAll('assoc');
        $c5r = $c5->fetchAll('assoc');
        $c6r = $c6->fetchAll('assoc');
        $c7r = $c7->fetchAll('assoc');
        $c8r = $c8->fetchAll('assoc');

        $this->set([
            'success' => true,
            'data' => [
                'lastposition' => [
                    '1' => $c1r['0']['val'],
                    '2' => $c2r['0']['val'],
                    '3' => $c3r['0']['val'],
                    '4' => $c4r['0']['val'],
                    '5' => $c5r['0']['val'],
                    '6' => $c6r['0']['val'],
                    '7' => $c7r['0']['val'],
                    '8' => $c8r['0']['val']
                ]
            ],
            '_serialize' => ['success', 'data']
        ]);
    }

    public function usemuscle(){

        $date = date('Y-m-d');
        $conn = ConnectionManager::get('default');
        $c1 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 1");
        $c2 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 2");
        $c3 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 3");
        $c4 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 4");
        $c5 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 5");
        $c6 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 6");
        $c7 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 7");
        $c8 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 8");

        $c1r = $c1->fetchAll('assoc');
        $c2r = $c2->fetchAll('assoc');
        $c3r = $c3->fetchAll('assoc');
        $c4r = $c4->fetchAll('assoc');
        $c5r = $c5->fetchAll('assoc');
        $c6r = $c6->fetchAll('assoc');
        $c7r = $c7->fetchAll('assoc');
        $c8r = $c8->fetchAll('assoc');

        //debug($c1r);die;
        $this->set([
            'success' => true,
            'data' => [
                'usemuscle' => [
                    '1' => $c1r['0']['AVG(val)'],
                    '2' => $c2r['0']['AVG(val)'],
                    '3' => $c3r['0']['AVG(val)'],
                    '4' => $c4r['0']['AVG(val)'],
                    '5' => $c5r['0']['AVG(val)'],
                    '6' => $c6r['0']['AVG(val)'],
                    '7' => $c7r['0']['AVG(val)'],
                    '8' => $c8r['0']['AVG(val)']
                ]
            ],
            '_serialize' => ['success', 'data']
        ]);

    }

    public function actualposition(){

        $user = '1';
        $conn = ConnectionManager::get('default');

        $c1 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 1 ORDER BY created DESC');
        $c2 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 2 ORDER BY created DESC');
        $c3 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 3 ORDER BY created DESC');
        $c4 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 4 ORDER BY created DESC');
        $c5 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 5 ORDER BY created DESC');
        $c6 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 6 ORDER BY created DESC');
        $c7 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 7 ORDER BY created DESC');
        $c8 = $conn->execute('SELECT id, val, pressionpoint_id, created FROM pressionpointshistories WHERE user_id = 1 AND pressionpoint_id = 8 ORDER BY created DESC');

        $c1r = $c1->fetchAll('assoc');
        $c2r = $c2->fetchAll('assoc');
        $c3r = $c3->fetchAll('assoc');
        $c4r = $c4->fetchAll('assoc');
        $c5r = $c5->fetchAll('assoc');
        $c6r = $c6->fetchAll('assoc');
        $c7r = $c7->fetchAll('assoc');
        $c8r = $c8->fetchAll('assoc');

        $this->set([
            'success' => true,
            'data' => [
                'actualposition' => [
                    '1' => $c1r['0']['val'],
                    '2' => $c2r['0']['val'],
                    '3' => $c3r['0']['val'],
                    '4' => $c4r['0']['val'],
                    '5' => $c5r['0']['val'],
                    '6' => $c6r['0']['val'],
                    '7' => $c7r['0']['val'],
                    '8' => $c8r['0']['val']
                ]
            ],
            '_serialize' => ['success', 'data']
        ]);

    }

    public function mystretching(){

        $date = date('Y-m-d');
        $conn = ConnectionManager::get('default');
        $c1 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 1");
        $c2 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 2");
        $c3 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 3");
        $c4 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 4");
        $c5 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 5");
        $c6 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 6");
        $c7 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 7");
        $c8 = $conn->execute("SELECT AVG(val) FROM pressionpointshistories WHERE created LIKE'%".$date."%' AND user_id = 1 AND pressionpoint_id = 8");

        $c1r = $c1->fetchAll('assoc');
        $c2r = $c2->fetchAll('assoc');
        $c3r = $c3->fetchAll('assoc');
        $c4r = $c4->fetchAll('assoc');
        $c5r = $c5->fetchAll('assoc');
        $c6r = $c6->fetchAll('assoc');
        $c7r = $c7->fetchAll('assoc');
        $c8r = $c8->fetchAll('assoc');

        $this->set([
            'success' => true,
            'data' => [
                'stretching' =>[
                    '0'=> [
                        'name' => 'Etirement des muscles dorsaux',
                        'description' => 'Petit programme pour attenuer les douleurs légères du dos',
                        'list' => [
                            '0' =>'S\'assoir au bord de la chaise et tendre les jambes',
                            '1' =>'Croiser les doigts au niveau de la nuque.',
                            '2' =>'Laisser la tête aller vers l’avant en la relâchant autant que possible',
                            '3' =>'Laisser les mains accentuer l’étirement vers l’avant et respirer profondément.',
                            '4' =>'Conserver la position plusieurs minutes.',
                        ],
                        'img' => 'http://sc.brzepka.ovh/img/stretchings/cmp.png',
                    ]
                ]
            ],
            '_serialize' => ['success', 'data']
        ]);

    }
}