<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pressionpoints Controller
 *
 * @property \App\Model\Table\PressionpointsTable $Pressionpoints
 */
class PressionpointsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pressionpoints = $this->paginate($this->Pressionpoints);

        $this->set(compact('pressionpoints'));
        $this->set('_serialize', ['pressionpoints']);
    }

    /**
     * View method
     *
     * @param string|null $id Pressionpoint id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pressionpoint = $this->Pressionpoints->get($id, [
            'contain' => ['Pressionpointshistories']
        ]);

        $this->set('pressionpoint', $pressionpoint);
        $this->set('_serialize', ['pressionpoint']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pressionpoint = $this->Pressionpoints->newEntity();
        if ($this->request->is('post')) {
            $pressionpoint = $this->Pressionpoints->patchEntity($pressionpoint, $this->request->data);
            if ($this->Pressionpoints->save($pressionpoint)) {
                $this->Flash->success(__('The pressionpoint has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pressionpoint could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pressionpoint'));
        $this->set('_serialize', ['pressionpoint']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pressionpoint id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pressionpoint = $this->Pressionpoints->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pressionpoint = $this->Pressionpoints->patchEntity($pressionpoint, $this->request->data);
            if ($this->Pressionpoints->save($pressionpoint)) {
                $this->Flash->success(__('The pressionpoint has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pressionpoint could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pressionpoint'));
        $this->set('_serialize', ['pressionpoint']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pressionpoint id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pressionpoint = $this->Pressionpoints->get($id);
        if ($this->Pressionpoints->delete($pressionpoint)) {
            $this->Flash->success(__('The pressionpoint has been deleted.'));
        } else {
            $this->Flash->error(__('The pressionpoint could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
