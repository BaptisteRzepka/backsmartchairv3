<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Stretchings Controller
 *
 * @property \App\Model\Table\StretchingsTable $Stretchings
 */
class StretchingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $stretchings = $this->paginate($this->Stretchings);

        $this->set(compact('stretchings'));
        $this->set('_serialize', ['stretchings']);
    }

    /**
     * View method
     *
     * @param string|null $id Stretching id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stretching = $this->Stretchings->get($id, [
            'contain' => []
        ]);

        $this->set('stretching', $stretching);
        $this->set('_serialize', ['stretching']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $stretching = $this->Stretchings->newEntity();
        if ($this->request->is('post')) {
            $stretching = $this->Stretchings->patchEntity($stretching, $this->request->data);
            if ($this->Stretchings->save($stretching)) {
                $this->Flash->success(__('The stretching has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The stretching could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('stretching'));
        $this->set('_serialize', ['stretching']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Stretching id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stretching = $this->Stretchings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stretching = $this->Stretchings->patchEntity($stretching, $this->request->data);
            if ($this->Stretchings->save($stretching)) {
                $this->Flash->success(__('The stretching has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The stretching could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('stretching'));
        $this->set('_serialize', ['stretching']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stretching id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stretching = $this->Stretchings->get($id);
        if ($this->Stretchings->delete($stretching)) {
            $this->Flash->success(__('The stretching has been deleted.'));
        } else {
            $this->Flash->error(__('The stretching could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
