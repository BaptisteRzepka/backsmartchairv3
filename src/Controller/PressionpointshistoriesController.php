<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pressionpointshistories Controller
 *
 * @property \App\Model\Table\PressionpointshistoriesTable $Pressionpointshistories
 */
class PressionpointshistoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Pressionpoints'],
            'order' => [
                'Pressionpoints.created' => 'desc'
            ]
        ];
        $pressionpointshistories = $this->paginate($this->Pressionpointshistories);

        $this->set(compact('pressionpointshistories'));
        $this->set('_serialize', ['pressionpointshistories']);
    }

    /**
     * View method
     *
     * @param string|null $id Pressionpointshistory id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pressionpointshistory = $this->Pressionpointshistories->get($id, [
            'contain' => ['Users', 'Pressionpoints']
        ]);

        $this->set('pressionpointshistory', $pressionpointshistory);
        $this->set('_serialize', ['pressionpointshistory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pressionpointshistory = $this->Pressionpointshistories->newEntity();
        if ($this->request->is('post')) {
            $pressionpointshistory = $this->Pressionpointshistories->patchEntity($pressionpointshistory, $this->request->data);
            if ($this->Pressionpointshistories->save($pressionpointshistory)) {
                $this->Flash->success(__('The pressionpointshistory has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pressionpointshistory could not be saved. Please, try again.'));
            }
        }
        $users = $this->Pressionpointshistories->Users->find('list', ['limit' => 200]);
        $pressionpoints = $this->Pressionpointshistories->Pressionpoints->find('list', ['limit' => 200]);
        $this->set(compact('pressionpointshistory', 'users', 'pressionpoints'));
        $this->set('_serialize', ['pressionpointshistory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pressionpointshistory id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pressionpointshistory = $this->Pressionpointshistories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pressionpointshistory = $this->Pressionpointshistories->patchEntity($pressionpointshistory, $this->request->data);
            if ($this->Pressionpointshistories->save($pressionpointshistory)) {
                $this->Flash->success(__('The pressionpointshistory has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pressionpointshistory could not be saved. Please, try again.'));
            }
        }
        $users = $this->Pressionpointshistories->Users->find('list', ['limit' => 200]);
        $pressionpoints = $this->Pressionpointshistories->Pressionpoints->find('list', ['limit' => 200]);
        $this->set(compact('pressionpointshistory', 'users', 'pressionpoints'));
        $this->set('_serialize', ['pressionpointshistory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pressionpointshistory id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pressionpointshistory = $this->Pressionpointshistories->get($id);
        if ($this->Pressionpointshistories->delete($pressionpointshistory)) {
            $this->Flash->success(__('The pressionpointshistory has been deleted.'));
        } else {
            $this->Flash->error(__('The pressionpointshistory could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
