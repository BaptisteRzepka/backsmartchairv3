<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PressionpointshistoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PressionpointshistoriesTable Test Case
 */
class PressionpointshistoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PressionpointshistoriesTable
     */
    public $Pressionpointshistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pressionpointshistories',
        'app.users',
        'app.pressionpoints'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pressionpointshistories') ? [] : ['className' => 'App\Model\Table\PressionpointshistoriesTable'];
        $this->Pressionpointshistories = TableRegistry::get('Pressionpointshistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pressionpointshistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
