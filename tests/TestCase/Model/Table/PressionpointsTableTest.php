<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PressionpointsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PressionpointsTable Test Case
 */
class PressionpointsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PressionpointsTable
     */
    public $Pressionpoints;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pressionpoints',
        'app.pressionpointshistories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pressionpoints') ? [] : ['className' => 'App\Model\Table\PressionpointsTable'];
        $this->Pressionpoints = TableRegistry::get('Pressionpoints', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pressionpoints);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
