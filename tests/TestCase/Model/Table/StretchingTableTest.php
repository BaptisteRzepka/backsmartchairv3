<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StretchingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StretchingTable Test Case
 */
class StretchingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StretchingTable
     */
    public $Stretching;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stretching'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Stretching') ? [] : ['className' => 'App\Model\Table\StretchingTable'];
        $this->Stretching = TableRegistry::get('Stretching', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Stretching);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
