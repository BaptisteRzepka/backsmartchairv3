<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StretchingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StretchingsTable Test Case
 */
class StretchingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StretchingsTable
     */
    public $Stretchings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stretchings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Stretchings') ? [] : ['className' => 'App\Model\Table\StretchingsTable'];
        $this->Stretchings = TableRegistry::get('Stretchings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Stretchings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
