<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'ADmad/JwtAuth' => $baseDir . '/vendor/admad/cakephp-jwt-auth/',
        'Alaxos' => $baseDir . '/vendor/alaxos/cakephp3-libs/',
        'Alaxos/BootstrapTheme' => $baseDir . '/vendor/alaxos/cakephp3-bootstrap-theme/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Cors' => $baseDir . '/vendor/ozee31/cakephp-cors/',
        'Crud' => $baseDir . '/vendor/friendsofcake/crud/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/'
    ]
];